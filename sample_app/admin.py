from django.contrib import admin

from .models import Food, Label, Activity, Meal, Exercise

admin.site.register(Food)
admin.site.register(Label)
admin.site.register(Activity)
admin.site.register(Meal)
admin.site.register(Exercise)
