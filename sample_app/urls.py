from django.urls import path
from .views import AddLabelView, ApproveActivityView, ApproveFoodView, RegisterView, LoginView, MealView, ExerciseView, NewFoodView, NewActivityView, DailyStatsView, WeeklyStatsView, MonthlyStatsView

urlpatterns = [
    path('register/', RegisterView.as_view(), name='register'),
    path('login/', LoginView.as_view(), name='login'),
    path('meal/', MealView.as_view(), name='meal'),
    path('exercise/', ExerciseView.as_view(), name='exercise'),
    path('new-food/', NewFoodView.as_view(), name='new-food'),
    path('label/', AddLabelView.as_view(), name='add-label'),
    path('new-activity/', NewActivityView.as_view(), name='new-activity'),
    path('foods/<int:pk>/approve/', ApproveFoodView.as_view(), name='approve_food'),
    path('activities/<int:pk>/approve/', ApproveActivityView.as_view(), name='approve_activity'),
    path('stats/daily/', DailyStatsView.as_view(), name='daily-stats'),
    path('stats/weekly/', WeeklyStatsView.as_view(), name='weekly-stats'),
    path('stats/monthly/', MonthlyStatsView.as_view(), name='monthly-stats'),
]