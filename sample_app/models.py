from django.db import models
from django.contrib.auth.models import User


class Label(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class Food(models.Model):
    name = models.CharField(max_length=255)
    calories = models.FloatField()
    labels = models.ManyToManyField('Label', related_name='foods')
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Meal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    food = models.ForeignKey(Food, on_delete=models.CASCADE)
    amount = models.FloatField()
    date = models.DateField()

    def __str__(self):
        return f'{self.user.username} {self.food.name} {self.amount} {self.date}'


class Activity(models.Model):
    name = models.CharField(max_length=255)
    calories = models.FloatField()
    is_approved = models.BooleanField(default=False)

    def __str__(self):
        return self.name


class Exercise(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE)
    duration = models.FloatField()
    date = models.DateField()

    def __str__(self):
        return f'{self.user.username} {self.activity.name} {self.duration} {self.date}'
