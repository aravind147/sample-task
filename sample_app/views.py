import calendar
import datetime
from rest_framework import views, permissions, generics
from rest_framework.response import Response
from django.contrib.auth import authenticate
from django.db.models import Sum
from rest_framework.permissions import IsAdminUser
from rest_framework import status
from .serializers import UserSerializer
from sample_app.models import Label, Food, Meal, Activity, Exercise
from .serializers import UserSerializer, LabelSerializer, FoodSerializer, MealSerializer, ActivitySerializer, ExerciseSerializer


class RegisterView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(views.APIView):
    def post(self, request):
        username = request.data.get("username")
        password = request.data.get("password")
        user = authenticate(username=username, password=password)
        if user:
            return Response({"token": user.auth_token.key})
        else:
            return Response({"error": "Wrong Credentials"}, status=status.HTTP_400)


class AddLabelView(generics.CreateAPIView):
    queryset = Label.objects.all()
    serializer_class = LabelSerializer
    permission_classes = (permissions.IsAuthenticated,)


class NewFoodView(generics.CreateAPIView):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(is_approved=False)


class MealView(generics.CreateAPIView):
    queryset = Meal.objects.all()
    serializer_class = MealSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class NewActivityView(generics.CreateAPIView):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    permission_classes = (permissions.IsAuthenticated,)


class ExerciseView(generics.CreateAPIView):
    queryset = Exercise.objects.all()
    serializer_class = ExerciseSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class ApproveFoodView(views.APIView):
    permission_classes = [IsAdminUser]

    def post(self, request, pk):
        food = Food.objects.get(pk=pk)
        food.is_approved = True
        food.save()
        return Response({'status': 'Food approved'})


class ApproveActivityView(views.APIView):
    permission_classes = [IsAdminUser]

    def post(self, request, pk):
        activity = Activity.objects.get(pk=pk)
        activity.is_approved = True
        activity.save()
        return Response({'status': 'Activity approved'})


class DailyStatsView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request):
        today = datetime.date.today()
        meals = Meal.objects.filter(date=today).aggregate(consumption=Sum('food__calories'))
        exercises = Exercise.objects.filter(date=today).aggregate(burnout=Sum('activity__calories'))
        return Response({'consumption': meals['consumption'], 'burnout': exercises['burnout']}, status=200)


class WeeklyStatsView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request):
        today = datetime.date.today()
        week_start = today - datetime.timedelta(days=today.weekday())
        week_end = week_start + datetime.timedelta(days=6)
        meals = Meal.objects.filter(date__range=[week_start, week_end]).aggregate(consumption=Sum('food__calories'))
        exercises = Exercise.objects.filter(date__range=[week_start, week_end]).aggregate(burnout=Sum('activity__calories'))
        return Response({'consumption': meals['consumption'], 'burnout': exercises['burnout']}, status=200)


class MonthlyStatsView(views.APIView):
    permission_classes = (permissions.IsAuthenticated,)
    def get(self, request):
        today = datetime.date.today()
        month_start = datetime.date(today.year, today.month, 1)
        month_end = datetime.date(today.year, today.month, calendar.monthrange(today.year, today.month)[1])
        meals = Meal.objects.filter(date__range=[month_start, month_end]).aggregate(consumption=Sum('food__calories'))
        exercises = Exercise.objects.filter(date__range=[month_start, month_end]).aggregate(burnout=Sum('activity__calories'))
        return Response({'consumption': meals['consumption'], 'burnout': exercises['burnout']}, status=200)